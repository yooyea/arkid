from django.apps import AppConfig


app_label = 'com_longgui_auto_form_fill'

class AutoFormFillAppConfig(AppConfig):
    name = app_label
