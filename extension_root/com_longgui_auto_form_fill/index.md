# 账密代填插件

## 功能介绍

通过此插件创建应用后，安装对应的chrome插件，可以实现将用于登录该应用的账号和密码存储在chrome插件里，点击应用图标，可以选择登录的账号，</br>
自动跳转到该应用的登录URL，并自动填入应用登录页账号和密码，实现自动登录，避免每次输入账号密码。</br>
另外，可以在chrome插件里做新增、修改、删除账号密码等管理操作。

### 配置账密代填应用

=== "创建代理URL应用"
    [![vLCUaQ.png](https://s1.ax1x.com/2022/09/09/vLCUaQ.png)](https://imgse.com/i/vLCUaQ)



=== "配置应用协议"
    [![vLCbZD.png](https://s1.ax1x.com/2022/09/09/vLCbZD.png)](https://imgse.com/i/vLCbZD)

    !!! 提示
        自动登录登录打开, 代表应用登录页自动填入账号密码后，自动点击登录按钮, 否则不自动点击登录按钮


=== "点击应用图标"
    [![vLPidg.png](https://s1.ax1x.com/2022/09/09/vLPidg.png)](https://imgse.com/i/vLPidg)
    
=== "安装chrome插件"
    如果没有安装过chrome插件，需要按页面提示先安装chrome插件，安装完成后，刷新页面
    [![vLPg6P.png](https://s1.ax1x.com/2022/09/09/vLPg6P.png)](https://imgse.com/i/vLPg6P)
    

=== "首次登录输入账号密码"
    [![vLiQjP.png](https://s1.ax1x.com/2022/09/09/vLiQjP.png)](https://imgse.com/i/vLiQjP)
    
    点击添加后，会自动跳转到应用登录页面
 

=== "跳转到应用登录页面"
    [![vLiDBT.png](https://s1.ax1x.com/2022/09/09/vLiDBT.png)](https://imgse.com/i/vLiDBT)


### 使用chrome插件管理账号

=== "添加应用账号"
    [![vLiIHO.png](https://s1.ax1x.com/2022/09/09/vLiIHO.png)](https://imgse.com/i/vLiIHO)

=== "重新点击应用图标"
    [![vLiH4H.png](https://s1.ax1x.com/2022/09/09/vLiH4H.png)](https://imgse.com/i/vLiH4H)
